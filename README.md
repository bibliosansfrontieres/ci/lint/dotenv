# CI : Lint Dotenv

Will lint every .env files in project using Rust package dotenv-linter.

By default, it will check all files beggining with .env.

[dotenv-linter documentation](https://dotenv-linter.github.io/#/?id=dotenv-linter)

## Usage

```
include:
  - project: 'bibliosansfrontieres/ci/lint/dotenv'
    file: 'template.yml'
```

## Override variables

|Variable|Description|Default|Required|
|-|-|-|-|
|DOTENV_FILES_TO_LINT|Specific .env files you want to lint, take all files beggining with .env by default| |no|
|DOTENV_FILES_TO_EXCLUDE|Specific .env files you want to exclude from linting| |no|

## Choose stage

By default, `lint_dotenv` uses `lint` stage.

You can override this by adding following code to your `.gitlab-ci.yml` :

```
lint_dotenv:
  stage: THE_STAGE_YOU_WANT_TO_USE
```
